package no.ntnu.idatg1002.playoff.model.utility;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import no.ntnu.idatg1002.playoff.model.Team;
import no.ntnu.idatg1002.playoff.model.TournamentManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FileHandlerTest {


  private TournamentManager manager;

  /**
   * Setting up and initialize a tournament manager object
   * before each test.
   */
  @BeforeEach
  void init(){
    List<Team> teams = new ArrayList<>();
    for(int i = 0;i<4;i++){
      teams.add(new Team("team"+i));
    }
    manager = new TournamentManager("Tournament",teams);
  }

  /**
   * Method to test reading and writing to files
   * under normal operations. When all values are
   * how they are supposed to be. No exception handling.
   */
  @Test
  void normalOperationReadingAndWritingToFileTest(){
    // Write tournament to file
    try{
      FileHandler.writeToFile(manager,new File("testerFile.csv"),0);
    }
    catch (IOException e){
      // Any problems writing to the file will cause a failed test
      fail();
    }
    // Make a new uninitialized tournament manager object
    TournamentManager sameManager;
    // Read from file and create a tournament manager object based on the file
    try {
      sameManager = FileHandler.readFromFile(Path.of("testerFile.csv"));
      // manager and sameManager should be the same
      assertEquals(manager,sameManager);
    }
    catch (IOException e){
      // Any problems reading a file will cause a failed test
      fail();
    }

  }

  /**
   * Testing to make sure the file handler class handles
   * problems in files in a proper way
   */
  @Test
  void testHandlingProblemsInFiles(){
    // Testing to make sure a tournament manager object will not be created if there
    // are no file with the specified filename,
    try{
      FileHandler.readFromFile(Path.of("notAFile.csv"));
      fail();
    }catch (IOException e){
      assertTrue(true);
    }

    // Throwing exception if the file format is not how it should be
    try{
      FileHandler.readFromFile(Path.of("src/main/resources/no/ntnu/idatg1002/playoff/formatFail.csv"));
      fail();
    }catch (IOException e){
      assertEquals("Illegal format in csv file. First line should contain the tournament name and 0 or 1",e.getMessage());
    }

  }

}