package no.ntnu.idatg1002.playoff.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Class to test for basic functionality
 * of the team class
 * @author Andreas Follevaag Malde
 * @version 1.0 - SNAPSHOT
 */
class TeamTest {

  private Team team;
  private ArrayList<String> players;

  /**
   * Method to initialize a team object and a player list.
   */
  @BeforeEach
  void init(){
    players = new ArrayList<>();
    players.addAll(Arrays.asList("Andreas", "Sander", "Nicolai", "Malin", "Eimen"));
    team = new Team("DreamTeam FC",players);
  }

  /**
   * Method to test the constructor to behave
   * as it should, both handling valid and invalid input
   */
  @Test
  void constructorTest(){
    Team team1 = new Team("TeamOne");
    assertEquals("TeamOne",team1.getName());
    assertEquals(0,team1.getPlayers().size());
    // Using the field variable "team"
    assertEquals(5,team.getPlayers().size());

    //Expecting class to throw exceptions when name value is ""
    try{
      new Team("");
      fail();
    }catch (IllegalArgumentException e){
      assertEquals("Name can not be blank or empty",e.getMessage());
    }
    //Expecting class to throw exceptions when the person list is null
    try{
      new Team("FailingTeam",null);
        fail();
    }catch(IllegalArgumentException e){
      assertEquals("Players list can not be null.",e.getMessage());
    }
  }


  /**
   * Method to test that the addTeamToPlayed method
   * work as expected
   */
  @Test
  void addTeamToPlayed() {
    // Initially there are no teams in teams played
    assertEquals(0,team.getTeamsPlayed().size());

    //Adding one team to teamsPlayed
    team.addTeamToPlayed(new Team("Another Team"));
    assertEquals(1,team.getTeamsPlayed().size());

    //Adding another team, expecting 2 teams in teams played
    team.addTeamToPlayed(new Team("FC Gjovik",players));
    assertEquals(2,team.getTeamsPlayed().size());

    // Expecting method to throw exception if a team is already in the list
    Team team1 = new Team("Another Team");
    try{
      team.addTeamToPlayed(team1);
      fail();
    }catch(IllegalArgumentException e){
      assertEquals("Operation failed... Check if team is already in the list.",e.getMessage());
    }

    // Expecting method to throw exception if trying to add itself
    try{
      team.addTeamToPlayed(team);
      fail();
    }catch (IllegalArgumentException e){
      assertEquals("Operation failed... Check if team is already in the list.",e.getMessage());
    }

    // Still expecting there to be 2 teams in the teams played list
    assertEquals(2,team.getTeamsPlayed().size());

  }

  /**
   * Test to make sure addGroupPoints work as expected
   */
  @Test
  void addGroupPoints() {
    // Group points are 0 at start
    assertEquals(0,team.getGroupPoints());

    // Adding 1 point to the team
    team.addGroupPoints(1);
    assertEquals(1,team.getGroupPoints());

    // Adding 3 points to the team
    team.addGroupPoints(3);
    assertEquals(4,team.getGroupPoints());

    // Method should throw exception if value between less than 0
    // or above 3 is inputted.
    try{
      team.addGroupPoints(-1);
      fail();
    }catch (IllegalArgumentException e){
      assertEquals("Points have to be between 0 and 3.",e.getMessage());
    }
    try{
      team.addGroupPoints(4);
      fail();
    }catch (IllegalArgumentException e){
      assertEquals("Points have to be between 0 and 3.",e.getMessage());
    }

    // Group points should still be 4
    assertEquals(4,team.getGroupPoints());
  }


  /**
   * Testing to make sure the equals' method
   * compare objects only based on the team name
   */
  @Test
  void testEquals() {
    Team team1 = new Team("DreamTeam FC");
    // Expecting team and team1 to be equal, since both their team names are the same
    assertEquals(team, team1);
    // Expecting team and team1 to not be equal, since their team names are the different
    team1.setTeamName("Not DreamTeam FC");
    assertNotEquals(team,team1);
  }
}