package no.ntnu.idatg1002.playoff.model.utility;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import no.ntnu.idatg1002.playoff.model.Team;
import no.ntnu.idatg1002.playoff.model.TournamentManager;


/**
 * Class for the applications file handler.
 */
public class FileHandler {
  private FileHandler() {
  }

  /**
   * Saves the teams from a tournament with a file.
   *
   * @param manager name of the tournament
   * @param file name of the file
   * @param num 0 or 1. 0 if on the teamManagerTab, 1 if in the playoff
   * @throws IOException the io exception
   */
  public static void writeToFile(TournamentManager manager, File file,int num) throws IOException {
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
      writer.write(manager.getTournamentName() + ","+num+"\n");
      for (Team team : manager.getListOfTeams()) {
        writer.write(team.getName() + "\n");
      }
    } catch (IOException e) {
      throw new IOException(e.getMessage());
    }
  }

  /**
   * Shows where the file is saved.
   *
   * @param file name of the file
   * @return the path of the file
   */
  public static Path getFilePath(File file) {
    return file.toPath();
  }

  /**
   * Reads a file with the structure for a tournament manager.
   *
   * @param path where the file is located
   * @return the name of the tournament manager and it's teams
   * @throws IOException the io exception
   */
  public static TournamentManager readFromFile(Path path) throws IOException {
    try (BufferedReader reader = new BufferedReader(new FileReader(path.toString()))) {
      String line = reader.readLine();
      String[] firstLine = line.split(",");
      if (firstLine.length != 2) {
        throw new IOException(
            "Illegal format in csv file. First line should contain the tournament name and 0 or 1");
      }
      TournamentManager manager = new TournamentManager(firstLine[0].strip());
      int value = Integer.parseInt(firstLine[1].strip());
      if(value == 1){
        manager.setTeamManager(true);
      }
      while ((line = reader.readLine()) != null) {
        String[] word = line.split(",");
        if (word.length != 1) {
          throw new IOException(
              "Illegal format in csv file. There should only be one team on each line.");
        }
        manager.addTeam(new Team(word[0].strip()));
      }
      return manager;
    } catch (IOException e) {
      throw new IOException(e.getMessage());
    }
  }
}
