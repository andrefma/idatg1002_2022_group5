package no.ntnu.idatg1002.playoff.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * this class represents a playOff system in the tournament, it takes care of the random matchmaking
 * mechanism and recording the history of the tournament.
 *
 * @author Eimen Oueslati, Andreas Malde
 * @version 1.0 - SNAPSHOT
 */

public class PlayOff {

  private List<Team> brackets;
  private List<Match> currentMatches;


  /**
   * constructor to initialize a PlayOff object and make sure that
   * the parameters passed are the correct format.
   *
   * @param teams a list of the teams to participate in the playoff
   * @param auto  Parameter deciding auto matchmaking or manual matchmaking. True = auto
   */

  public PlayOff(List<Team> teams, boolean auto) {
    //throw an exception if the teams list does not have 2, 4, 8, 16, 32 teams
    if (teams.size() < 0 || teams.size() > 32
        || java.lang.Math.log(teams.size()) / java.lang.Math.log(2)
        != (int) (java.lang.Math.log(teams.size()) / java.lang.Math.log(2))) {
      throw new IllegalArgumentException("the qualified teams list must be of adequate size");
    }
    //throw an exception if the list have two identical elements
    if (checkForDuplicate(teams)) {
      throw new IllegalArgumentException("the list of teams can not have duplicates");
    }

    brackets = teams;
    if (auto) {
      Collections.shuffle(brackets);
    }
    currentMatches = new ArrayList<>();
    setupMatches();
  }


  /**
   * this method iterates through an Arraylist to check if there is any duplicate elements.
   *
   * @param array the array to be checked
   * @return true in case there is a duplicate or false if there is none
   */
  public Boolean checkForDuplicate(List<Team> array) {
    for (int i = 0; i != array.size() - 1; i++) {
      for (int j = i + 1; j != array.size() - 1; j++) {
        if (array.get(i).equals(array.get(j))) {
          return true;
        }
      }
    }
    return false;
  }


  /**
   * this method is called at the start of the tournament or
   * whenever a phase of the playoff is done. It registers
   * the previous matches and updates the current matches
   * with the teams that moves forward in the tournament.
   */
  private void setupMatches() {

    //clear the currentMatches list from the previous round to add the new matches
    currentMatches.clear();

    //iterate through the brackets to update the current matches
    if (brackets.size() > 1) {
      for (int i = 0; i != brackets.size(); i = i + 2) {
        currentMatches.add(new Match(brackets.get(i), brackets.get(i + 1)));
      }
    }

  }

  /**
   * Checker to make sure that the match have not ended in a draw.
   *
   * @return returns true/false depending on whether there's a draw or not
   */
  public boolean checkNoDraw() {
    for (Match match : currentMatches) {
      if (match.getLoser() == null) {
        return false;
      }
    }
    return true;
  }


  /**
   * this method is responsible for updating the brackets and removing
   * the teams that were eliminated.
   */
  public void updateBrackets() {
    //iterate through current matches list to eliminate the teams that lost
    for (Match match : currentMatches) {
      boolean found = false;
      int index = 0;
      while (!found && index < brackets.size()) {
        if (brackets.get(index).equals(match.getLoser())) {
          brackets.remove(brackets.get(index));
          found = true;
        }
        index++;
      }
    }
    setupMatches();
  }

  /**
   * this method is called at the end of the tournament when only one team is left in the playoff.
   */
  public Team getWinner() throws IllegalStateException {
    //throws an exception if there is more than one team in the brackets
    if (brackets.size() != 1) {
      throw new IllegalStateException("the tournament has not ended yet");
    }

    //returns the one team left in the bracket after all the other teams has been eliminatesd
    return brackets.get(0);
  }

  /**
   * Getter for the brackets.
   *
   * @return return the list of the teams that has not been eliminated yet
   */
  public List<Team> getBrackets() {
    return brackets;
  }

  /**
   * Getter for the teams fighting in the first bracket.
   *
   * @return returns the teams fighting in the first bracket
   */
  public List<Match> getFirstBracket() {
    List<Match> firstBracket = new ArrayList<>();
    for (int i = 0; i < (this.currentMatches.size() / 2); i++) {
      firstBracket.add(this.currentMatches.get(i));
    }
    return firstBracket;
  }

  /**
   * Getter for the teams fighting in the second bracket.
   *
   * @return returns the teams fighting in the second bracket
   */
  public List<Match> getSecondBracket() {
    List<Match> secondBracket = new ArrayList<>();
    for (int i = this.currentMatches.size() - 1; i > (this.currentMatches.size() / 2 - 1); i--) {
      secondBracket.add(this.currentMatches.get(i));
    }
    return secondBracket;
  }

  /**
   * Getter for the current matches.
   *
   * @return return the list of the current matches
   */
  public List<Match> getCurrentMatches() {
    //
    return currentMatches;
  }



}
