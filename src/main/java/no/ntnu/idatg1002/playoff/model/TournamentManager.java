package no.ntnu.idatg1002.playoff.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * The class representing the managing of the tournament application.
 */
public class TournamentManager {
  private String tournamentName;
  private List<Team> teamArrayList;
  private boolean teamManager;

  /**
   * Instantiates a new Tournament manager.
   *
   * @param tournamentName the tournament name
   */
  public TournamentManager(String tournamentName) {
    this.setTournamentName(tournamentName);
    teamArrayList = new ArrayList<>();
    teamManager = false;
  }

  /**
   * Instantiates a new Tournament manager.
   *
   * @param tournamentName the tournament name
   */
  public TournamentManager(String tournamentName, List<Team> teamList) {
    this.setTournamentName(tournamentName);
    this.teamArrayList = teamList;
    teamManager = false;
  }

  /**
   * Gets tournament name.
   *
   * @return the tournament name to get
   */
  public String getTournamentName() {
    return tournamentName;
  }

  /**
   * Gets list of teams.
   *
   * @return list representation of all teams.
   */
  public List<Team> getListOfTeams() {
    return teamArrayList;
  }

  /**
   * Sets tournament name.
   *
   * @param tournamentName the tournament name to get
   */
  public void setTournamentName(String tournamentName) {
    if (tournamentName.isEmpty() || tournamentName.isBlank()) {
      throw new IllegalArgumentException("Tournament name can not be blank.");
    }
    this.tournamentName = tournamentName;
  }

  /**
   * Set the team manager value.
   * @param value True / false
   */
  public void setTeamManager(boolean value){
    teamManager = value;
  }

  /**
   * @return the team manager value
   */
  public boolean getTeamManagerValue(){
    return teamManager;
  }

  /**
   * Method for adding a team to the application.
   *
   * @param teamsToAdd the teams to add
   */
  public void addTeam(Team teamsToAdd) {
    if (teamArrayList.contains(teamsToAdd)) {
      throw new IllegalArgumentException("Team already added to the tournament");
    }
    teamArrayList.add(teamsToAdd);

  }

  /**
   * Method to make sure there are a valid amount of teams in
   * the list before matchmaking.
   *
   * @return true if there are 4,8,16 or 32 teams in the list
   */
  public boolean isValidTeamList() {
    int[] validSize = {4, 8, 16, 32};

    for (int size : validSize) {
      if (teamArrayList.size() == size) {
        return true;
      }
    }
    return false;
  }

  /**
   * Method to check if there is a team with
   * the same name as the one given in the parameter.
   *
   * @param name Team name to search for
   * @return True if there is a team in the list with the specified team name
   */
  public boolean isTeamNameInList(String name) {
    for (Team team : teamArrayList) {
      if (team.getName().equalsIgnoreCase(name)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Method for removing team from the application.
   *
   * @param teamToRemove the team to remove
   */
  public void removeTeam(Team teamToRemove) {
    if (!teamArrayList.contains(teamToRemove)) {
      throw new IllegalArgumentException("Team not registered to the tournament");
    }
    teamArrayList.remove(teamToRemove);
  }

  /**
   * Makes a list of teams.
   *
   * @param listOfTeams arraylist of teams
   */
  public void setTeamList(List<Team> listOfTeams) {
    if (!listOfTeams.isEmpty()) {
      teamArrayList = listOfTeams;
    } else {
      throw new IllegalArgumentException("List is invalid!");
    }
  }

  /**
   * Comparing tournament objects based on their
   * name and teams.
   *
   * @param o Object to compare against
   * @return True / False
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TournamentManager that = (TournamentManager) o;
    return Objects.equals(tournamentName, that.tournamentName)
        && Objects.equals(teamArrayList, that.teamArrayList);
  }

  /**
   * Returns a hashcode for the tournament name and team list.
   *
   * @return Hashcode based on the tournament name and the team list.
   */
  @Override
  public int hashCode() {
    return Objects.hash(tournamentName, teamArrayList);
  }
}
