package no.ntnu.idatg1002.playoff;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Stage;


/**
 * The method for starting the application, and GUI elements.
 */
public class Main extends Application {
  @Override
  public void start(Stage stage) throws IOException {
    FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("main-view.fxml"));
    Scene scene = new Scene(fxmlLoader.load(), 600, 400);
    stage.getIcons().add(new Image(String.valueOf(
        Main.class.getResource("controllers/PlayOffOnlyLogo.png"))));
    stage.setTitle("PlayOff - Tournament Manager");
    stage.setScene(scene);
    stage.setResizable(false);
    stage.show();

    stage.setOnCloseRequest(evt -> {
      exitConfirmation(stage);
      evt.consume();
    });
  }

  /**
   * Confirmation for exiting application.
   * Stops the user from accidentally exiting application by pressing the X
   * in the top right corner.
   *
   * @param stage The stage to exit.
   */
  public static void exitConfirmation(Stage stage) {
    // allow user to decide between yes and no
    Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
        "Do you really want to close this application?",
        ButtonType.YES, ButtonType.NO);

    // clicking X also means no
    ButtonType result = alert.showAndWait().orElse(ButtonType.NO);

    if (!ButtonType.NO.equals(result)) {
      stage.close();
    }
  }

  /**
   * The entry point of application.
   *
   * @param args the input arguments
   */
  public static void main(String[] args) {
    launch();
  }
}