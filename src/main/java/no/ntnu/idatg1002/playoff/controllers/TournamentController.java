package no.ntnu.idatg1002.playoff.controllers;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import no.ntnu.idatg1002.playoff.Main;
import no.ntnu.idatg1002.playoff.model.Match;
import no.ntnu.idatg1002.playoff.model.PlayOff;
import no.ntnu.idatg1002.playoff.model.Team;
import no.ntnu.idatg1002.playoff.model.TournamentManager;
import no.ntnu.idatg1002.playoff.model.utility.FileHandler;

/**
 * Main Tournament controller class to connect the
 * GUI to the backend model.
 *
 * @author Andreas Follevaag Malde, Sander Hauge
 * @version 1.0 - SNAPSHOT
 */
public class TournamentController implements Initializable {
  @FXML private ImageView setScoreLogo;
  @FXML private ImageView nextRoundLogo;
  @FXML private ImageView getWinnerLogo;
  @FXML private ImageView addTeamLogo;
  @FXML private ImageView editTeamLogo;
  @FXML private ImageView removeTeamLogo;
  @FXML private Label tournamentName;
  @FXML private TableView<Team> teamTable;
  @FXML private TableColumn<Team, String> teamColumn;
  @FXML private Tab matchmakerTab;
  @FXML private Label amountTeams;
  @FXML private TabPane tabPane;
  @FXML private Tab teamManagerTab;
  @FXML private Label roundCounter;
  // Bracket one Table
  @FXML private TableColumn<Match, Integer> team1ScoreColumn;
  @FXML private TableColumn<Match, Integer> team2ScoreColumn;
  @FXML private TableView<Match> bracketOneTableView;
  @FXML private TableColumn<Match, String> bracketOneTableColumn;
  // Bracket two Table
  @FXML private TableView<Match> bracketTwoTableView;
  @FXML private TableColumn<Match, String> bracketTwoTableColumn;
  @FXML private TableColumn<Match, Integer> bracketTwoTeamOneScore;
  @FXML private TableColumn<Match, Integer> bracketTwoTeamTwoScore;
  @FXML private Tab finalTab;
  @FXML private Label firstFinalTeam;
  @FXML private Label secondFinalTeam;
  @FXML private TextField firstFinalTeamScore;
  @FXML private TextField secondFinalTeamScore;
  @FXML private Tab winnerTab;
  @FXML private Label winnerLabel;

  private TournamentManager tournamentManager;
  private ObservableList<Team> observableListOfTeams;
  private PlayOff playOff;

  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {
    addTeamLogo.setImage(new Image(String.valueOf(TournamentController.class.getResource("list-plus.png"))));
    editTeamLogo.setImage(new Image(String.valueOf(TournamentController.class.getResource("edit.png"))));
    removeTeamLogo.setImage(new Image(String.valueOf(TournamentController.class.getResource("list-minus.png"))));
    setScoreLogo.setImage(new Image(String.valueOf(TournamentController.class.getResource("edit-3.png"))));
    nextRoundLogo.setImage(new Image(String.valueOf(TournamentController.class.getResource("arrow-big-right.png"))));
    getWinnerLogo.setImage(new Image(String.valueOf(TournamentController.class.getResource("winner.png"))));
    observableListOfTeams = FXCollections.observableList(new ArrayList<>());
    textFieldListener(firstFinalTeamScore);
    textFieldListener(secondFinalTeamScore);
  }

  /**
   * Method for opening an ongoing tournament from an existing file.
   *
   * @param tm the existing tournament
   */
  public void openFromFile(TournamentManager tm) {
    tournamentManager = tm;
    observableListOfTeams.addAll(tournamentManager.getListOfTeams());
    tournamentManager.setTeamList(observableListOfTeams); //Find a better solution.

    teamTable.setItems(observableListOfTeams);

    teamColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
    tournamentName.setText(tournamentManager.getTournamentName());
    amountTeams.setText(String.valueOf(tournamentManager.getListOfTeams().size()));

    if(tournamentManager.getTeamManagerValue()){
      playOff = new PlayOff(tournamentManager.getListOfTeams(),false);
      switch (tournamentManager.getListOfTeams().size()){
        case 1:
          // Winning screen
          winnerScreen();
          break;
        case 2:
          // Finale
          tournamentFinal();
          break;
        default:
          this.getToPlayOff();
          break;
      }
    }
  }

  /**
   * Action handler for button press on about tap in help menu.
   */
  @FXML
  private void onAboutButtonClicked() {
    Alert alert = new Alert(Alert.AlertType.INFORMATION, "About");
    alert.setContentText(
        "This is an application made by Sander, Eimen, Andreas, Malin\nand Nicolai!");
    alert.setTitle("About");
    alert.setHeaderText("This is a tournament manager for Football.");
    alert.showAndWait();
  }

  /**
   * Method for creating a new tournament.
   *
   * @param name the name
   */
  public void newTournament(String name) {
    tournamentManager = new TournamentManager(name, observableListOfTeams);
    teamTable.setItems(observableListOfTeams);

    teamColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
    tournamentName.setText(tournamentManager.getTournamentName());
  }

  /**
   * When back button pressed, user will
   * be sent to main page.
   *
   * @throws IOException Throws exception if there is a faulty input.
   */
  @FXML
  private void onBackButtonPressed() throws IOException {
    if (confirmationAlert("Do you really want to go back?")) {
      Stage stage = (Stage) tournamentName.getScene().getWindow();
      FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("main-view.fxml"));
      Scene scene = new Scene(fxmlLoader.load(), 600, 400);
      stage.setScene(scene);
    }
  }

  /**
   * Method which deletes selected entry in list.
   */
  @FXML
  private void removeTeamButtonClicked() {
    try {
      Team teamToRemove = teamTable.getSelectionModel().getSelectedItem();
      if (teamToRemove != null) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
            "Are you sure you want to delete " + teamToRemove.getName() + "?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
          tournamentManager.removeTeam(teamToRemove);
          amountTeams.setText(String.valueOf(tournamentManager.getListOfTeams().size()));
        }
      }
    } catch (Exception e) {
      Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
      alert.setHeaderText("Team does not exist.");
      alert.showAndWait();
    }
  }

  /**
   * Adds new team when button is pressed.
   * Future implementation: open a dialog to prompt user to insert name etc.
   */
  @FXML
  private void addATeamButtonClicked() {
    TextInputDialog teamName = new TextInputDialog();
    teamName.setTitle("Add team");
    teamName.setHeaderText("Enter team name:");
    Optional <String> results = teamName.showAndWait();
    if(results.isPresent()) {
      String name = teamName.getResult();

      try {
        Team team = new Team(name);
        tournamentManager.addTeam(team);
      } catch (IllegalArgumentException e) {
        new Alert(Alert.AlertType.ERROR, e.getMessage()).showAndWait();
      }
      amountTeams.setText(String.valueOf(tournamentManager.getListOfTeams().size()));
    }
  }

  /**
   * Edits a team's name.
   */
  @FXML
  private void editTeamButtonClicked() {
    try {
      Team teamToEdit = teamTable.getSelectionModel().getSelectedItem();
      if (teamToEdit != null) {
        TextInputDialog newTeamName = new TextInputDialog();
        newTeamName.setTitle("Edit team");
        newTeamName.setHeaderText("Enter new team name");
        Optional<String> results = newTeamName.showAndWait();
        // Check which button is pressed
        if (results.isPresent()) {
          String name = newTeamName.getResult();
          // If there are already a team in the list with that name, a warning is displayed.
          if (!tournamentManager.isTeamNameInList(name)) {
            teamToEdit.setTeamName(name);
            teamTable.refresh();
          } else {
            new Alert(Alert.AlertType.ERROR, "Team is already in the list").showAndWait();
          }

        }
      }
    } catch (Exception e) {
      new Alert(Alert.AlertType.ERROR, e.getMessage()).showAndWait();
    }
  }

  /**
   * Action handler which will allow user into matchmaker tab
   * when user has 2,4,8,16 or 32 teams added.
   * See comments below for further functions.
   */
  @FXML
  private void matchmakeButtonPressed(ActionEvent event) {
    if (tournamentManager.isValidTeamList()) {
      matchmakerTab.setDisable(false);
      tabPane.getSelectionModel().select(matchmakerTab);
      teamManagerTab.setDisable(true);


      Button matchmakerButton = (Button) event.getSource();
      if (matchmakerButton.getId().equals("autoMatchmakeButton")) {
        //Make new playoff here where teams are randomly shuffled
        playOff = new PlayOff(tournamentManager.getListOfTeams(), true);
      } else {
        //Make new playoff here where teams are in the order they were added
        playOff = new PlayOff(tournamentManager.getListOfTeams(), false);
      }

      setPlayOffTables();

    } else {
      // Alert
      Alert alert = new Alert(Alert.AlertType.WARNING);
      alert.setTitle("Invalid amount of teams");
      alert.setHeaderText("Invalid amount of teams");
      alert.setContentText("The team list has to contain 4, 8, 16 or 32 teams\n"
          + "to continue matchmaking");
      alert.showAndWait();
    }
  }

  /**
   * Set up the tables in the playoff window to show matches in
   * both brackets
   */
  private void setPlayOffTables(){
    bracketOneTableView.setItems(FXCollections.observableList(playOff.getFirstBracket()));
    bracketOneTableColumn.setCellValueFactory(new PropertyValueFactory<>("representation"));
    team1ScoreColumn.setCellValueFactory(new PropertyValueFactory<>("teamOneScore"));
    team2ScoreColumn.setCellValueFactory(new PropertyValueFactory<>("teamTwoScore"));


    bracketTwoTableView.setItems(FXCollections.observableList(playOff.getSecondBracket()));
    bracketTwoTableColumn.setCellValueFactory(new PropertyValueFactory<>("representation"));
    bracketTwoTeamOneScore.setCellValueFactory(new PropertyValueFactory<>("teamOneScore"));
    bracketTwoTeamTwoScore.setCellValueFactory(new PropertyValueFactory<>("teamTwoScore"));
  }

  /**
   * Get directly to the playoff after reading from
   * file
   */
  private void getToPlayOff() {
    if (tournamentManager.isValidTeamList()) {
      matchmakerTab.setDisable(false);
      tabPane.getSelectionModel().select(matchmakerTab);
      teamManagerTab.setDisable(true);

      setPlayOffTables();

    } else {
      // Alert
      Alert alert = new Alert(Alert.AlertType.WARNING);
      alert.setTitle("Invalid amount of teams");
      alert.setHeaderText("Invalid amount of teams");
      alert.setContentText("The team list has to contain 4, 8, 16 or 32 teams\n"
          + "to continue matchmaking");
      alert.showAndWait();
    }
  }



  /**
   * Updating the tournament final tab to display
   * the teams in the final.
   */
  private void tournamentFinal() {
    finalTab.setDisable(false);
    tabPane.getSelectionModel().select(finalTab);
    matchmakerTab.setDisable(true);
    teamManagerTab.setDisable(true);

    Match finalMatch = playOff.getCurrentMatches().get(0);
    firstFinalTeam.setText(finalMatch.getTeamOne().getName());
    secondFinalTeam.setText(finalMatch.getTeamTwo().getName());
  }

  /**
   * ActionHandler which should use updateMatches from PlayOff
   * class and update columns, then increment round.
   */
  public void nextRoundButtonClicked() {
    if (playOff.checkNoDraw()) {
      if (playOff.getCurrentMatches().size() == 2) {
        playOff.updateBrackets();
        this.tournamentFinal();

      } else {
        int i = Integer.parseInt(roundCounter.getText());
        roundCounter.setText(String.valueOf(i + 1));
        //Update bracket and delete the teams that lost
        playOff.updateBrackets();

        //Update the tableView to show new matches
        bracketOneTableView.setItems(FXCollections.observableList(playOff.getFirstBracket()));
        bracketTwoTableView.setItems(FXCollections.observableList(playOff.getSecondBracket()));
      }
    } else {
      Alert alert = new Alert(Alert.AlertType.WARNING);
      alert.setHeaderText("There can be no draws in playoff");
      alert.setContentText("Make sure every match has a winner.\n"
          + "Register total amount of goals if there has been a penalty shootout.");
      alert.showAndWait();
    }
  }

  /**
   * Action handler, when cancel is pressed the playoff
   * object should be scrapped and user sent to team manager tab.
   */
  public void cancelTournamentButtonClicked() {
    if (confirmationAlert("Do you really want to cancel tournament?")) {
      matchmakerTab.setDisable(true);
      tabPane.getSelectionModel().select(teamManagerTab);
      teamManagerTab.setDisable(false);
    }
  }

  /**
   * Method which prompts user if they want to go back.
   *
   * @param whatToConfirm String, custom prompt.
   * @return Boolean, returns true if OK is pressed.
   */
  private boolean confirmationAlert(String whatToConfirm) {
    Alert confirmCancel = new Alert(Alert.AlertType.CONFIRMATION);
    confirmCancel.setTitle("Are you sure?");
    confirmCancel.setHeaderText(whatToConfirm);
    Optional<ButtonType> result = confirmCancel.showAndWait();
    if(result.isPresent() && result.get() == ButtonType.OK){
      return true;
    }else{
      confirmCancel.close();
      return false;
    }
  }

  /**
   * Action handler which when a selected entry in list
   * a window will pop up and allow user to input score for
   * corresponding match.
   */
  public void setScoreButtonClicked() {
    Match selectedMatchFirstBracket = bracketOneTableView.getSelectionModel().getSelectedItem();
    Match selectedMatchSecondBracket = bracketTwoTableView.getSelectionModel().getSelectedItem();
    if (selectedMatchFirstBracket != null && selectedMatchSecondBracket != null) {
      bracketOneTableView.getSelectionModel().clearSelection();
      bracketTwoTableView.getSelectionModel().clearSelection();
    }
    else if (selectedMatchFirstBracket == null && selectedMatchSecondBracket == null)
    {
      new Alert(Alert.AlertType.WARNING, "please select a match before pressing the set score").showAndWait();
    }
    else {
      TextInputDialog firstTeamDialog = new TextInputDialog();
      textFieldListener(firstTeamDialog.getEditor());
      firstTeamDialog.setHeaderText("First team score:");
      firstTeamDialog.showAndWait();
      String firstTeamAnswer = firstTeamDialog.getResult();
      TextInputDialog secondTeamDialog = new TextInputDialog();
      textFieldListener(secondTeamDialog.getEditor());
      secondTeamDialog.setHeaderText("Second team score:");
      secondTeamDialog.showAndWait();
      String secondTeamAnswer = secondTeamDialog.getResult();

      try {
        // Get the selected table row to set the score of the matches.
        if (selectedMatchFirstBracket == null && selectedMatchSecondBracket != null) {
          selectedMatchSecondBracket.setScore(Integer.parseInt(firstTeamAnswer),
              Integer.parseInt(secondTeamAnswer));
          bracketTwoTableView.getSelectionModel().clearSelection();
        } else if (selectedMatchFirstBracket != null) {
          selectedMatchFirstBracket.setScore(Integer.parseInt(firstTeamAnswer),
              Integer.parseInt(secondTeamAnswer));
          bracketOneTableView.getSelectionModel().clearSelection();
        }

        //Refresh the tableviews
        bracketOneTableView.refresh();
        bracketTwoTableView.refresh();
      } catch (NumberFormatException e) {
        new Alert(Alert.AlertType.ERROR, "Insert valid numbers. No text").showAndWait();
      } catch (IllegalArgumentException e) {
        new Alert(Alert.AlertType.ERROR, e.getMessage()).showAndWait();
      }
    }
  }

  /**
   * Winner screen. Will display the name of the
   * winner to the screen.
   */
  private void winnerScreen() {
    winnerTab.setDisable(false);
    tabPane.getSelectionModel().select(winnerTab);
    finalTab.setDisable(true);
    teamManagerTab.setDisable(true);
    // Display the tournament winner to the screen
    winnerLabel.setText(playOff.getWinner().getName());
  }

  /**
   * Method which restricts text field input to only being ints.
   *
   * @param textField TextField in GUI.
   */
  private void textFieldListener(TextField textField) {
  ChangeListener<String> cl = (observableValue, oldValue, newValue) -> {
  if (!newValue.matches("\\d")) {
  textField.setText(newValue.replaceAll("[^\\d]", ""));
    }
  };
  textField.textProperty().addListener(cl);
  }

  /**
   * Set the result of the final and determine the
   * winner of the tournament.
   */
  public void getTournamentWinner() {
    try {
      int teamOneScore = Integer.parseInt((firstFinalTeamScore.getText()));
      int teamTwoScore = Integer.parseInt((secondFinalTeamScore.getText()));
      if (teamOneScore != teamTwoScore) {
        playOff.getCurrentMatches().get(0).setScore(teamOneScore, teamTwoScore);
        playOff.updateBrackets();
        this.winnerScreen();
      } else {
        new Alert(Alert.AlertType.WARNING, "There can not be a draw in the final").showAndWait();
      }
    } catch (NumberFormatException e) {
      new Alert(Alert.AlertType.WARNING, "Insert valid numbers. No text").showAndWait();
    }
  }

  /**
   * Close the application when the close button
   * at the winner screen is pressed.
   */
  public void winnerClose() {
    Main.exitConfirmation((Stage) tournamentName.getScene().getWindow());
  }

  /**
   * On save button pressed.
   */
  public void onSaveButtonPressed() {
    try {
      FileChooser chooser = new FileChooser();
      chooser.setInitialFileName(tournamentName.getText().strip());
      chooser.getExtensionFilters()
          .addAll(new FileChooser.ExtensionFilter("*.csv", "Comma Separated File"));
      File file = chooser.showSaveDialog(tournamentName.getScene().getWindow());
      chooser.setInitialDirectory(file.getParentFile()); //Save chosen directory.
      if(tabPane.getSelectionModel().getSelectedItem()==teamManagerTab){
        FileHandler.writeToFile(tournamentManager, file,0);
      }else{
        FileHandler.writeToFile(tournamentManager, file,1);
      }
    } catch (Exception e) {
      Alert alert = new Alert(Alert.AlertType.ERROR);
      alert.setHeaderText("File saving went wrong.");
      alert.setContentText(e.getMessage());
      alert.showAndWait();
    }
  }

  /**
   * A pop-up explaining the difference between
   * auto matchmaking and manual matchmaking
   */
  public void informationButtonPressed() {
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    alert.setHeaderText("Auto or Manual matchmaking");
    alert.setContentText("There are two ways to arrange which teams are\n" +
        "playing against each other.\n" +
        "AUTO MATCHMAKE:\nPressing the auto matchmake button simply shuffles" +
        " the team list making it fully random who will faces each other in" +
        " the first match.\n" +
        "MANUAL MATCHMAKE:\nPressing this button will arrange matches based on" +
        " when teams where added to the tournament. The first and the second team" +
        " will meet. The third and fourth, and so on.");
    alert.showAndWait();
  }
}
