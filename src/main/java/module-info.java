module no.ntnu.idatg1002.playoff {
  requires javafx.controls;
  requires javafx.fxml;


  opens no.ntnu.idatg1002.playoff;
  exports no.ntnu.idatg1002.playoff;
  exports no.ntnu.idatg1002.playoff.controllers;
  exports no.ntnu.idatg1002.playoff.model;
  opens no.ntnu.idatg1002.playoff.model;
  opens no.ntnu.idatg1002.playoff.controllers;
}